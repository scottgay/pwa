/**
 * Created by rvasudevan on 5/9/16.
 */

var myCacheIndex = '22'; //R: use this for increements
var dataCacheName = 'weatherData-'+ myCacheIndex;

//TODO: this caching for non-prod code only
var cacheName = 'weatherPWA-step-' +  myCacheIndex;
var filesToCache = [ //appshell components or files.
 // '/',
  'index.html',
  'app.js', //moved this from /scripts to root since it did not work under scripts.
  'styles/inline.css',
  'images/clear.png',
  'images/cloudy-scattered-showers.png',
  'images/cloudy.png',
  'images/fog.png',
  'images/ic\_add\_white\_24px.svg',
  'images/ic\_refresh\_white\_24px.svg',
  'images/partly-cloudy.png',
  'images/rain.png',
  'images/scattered-showers.png',
  'images/sleet.png',
  'images/snow.png',
  'images/thunderstorm.png',
  'images/wind.png'
];

//self refers to the ServiceWorkerGlobalScope object: the service worker itself.
self.addEventListener('install', function(e) {
  console.log('[ServiceWorker] Install');
  e.waitUntil(
      caches.open(cacheName).then(function(cache) {
        console.log('[ServiceWorker] Caching app shell');
        return cache.addAll(filesToCache); //atomic. will fail all even if one file fails.
      })
  );
});


self.addEventListener('activate', function(e) {
  console.log('[ServiceWorker] Activate');
  e.waitUntil(
      caches.keys().then(function(keyList) {
        return Promise.all(keyList.map(function(key) {
          console.log('[ServiceWorker] Removing old cache', key);
          if (key !== cacheName) {
            return caches.delete(key);
          }
        }));
      })
  );
});

/*self.addEventListener('fetch', function(e) {
  console.log('[ServiceWorker] Fetch', e.request.url);
  e.respondWith(
      caches.match(e.request).then(function(response) {
        return response || fetch(e.request);
      })
  );
});*/

//cache first then network.
self.addEventListener('fetch', function(e) {
  console.log('[ServiceWorker] Fetch', e.request.url);
  var dataUrl = 'https://publicdata-weather.firebaseio.com/';
  if (e.request.url.indexOf(dataUrl) === 0) { //check url; fetch and cache for later use.
    e.respondWith(
        fetch(e.request)
            .then(function(response) {
              return caches.open(dataCacheName).then(function(cache) {
                cache.put(e.request.url, response.clone());
                console.log('[ServiceWorker] Fetched&Cached Data');
                return response;
              });
            })
    );
  } else {
    e.respondWith( //return from cache if it exists.
       caches.match(e.request).then(function(response) {
          return response || fetch(e.request);
        })
    );
  }
});
